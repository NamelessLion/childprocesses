from tkinter.constants import FALSE, TRUE
import psutil
# Se requiere:
# lista de pid
# lista de PID del proceso 0
# lista de PID del proceso 1
# lista de PID del proceso 2
# exluir los PID de los procesos 0 1 2
# filtar PID por procesos padre
# Obtener los datos del proceso
#
# lista de procesos
list_pids = psutil.pids()
# lista de procesos 1
list_pid_one = psutil.Process(1).children(recursive=False)
# lista de procesos 2
list_pid_two = psutil.Process(2).children(recursive=False)

list_clear_pids = []


def search(list, PID):
    dato = 0
    for index in range(len(list)):
        dato = index
        if list[index].pid == PID:
            return {"action": True, "index": dato}
    return {"action": False, "index": dato}


for index in range(len(list_pids)):
    response_one = search(list_pid_one, list_pids[index])
    response_two = search(list_pid_two, list_pids[index])
    if (response_two['action'] == False and response_one['action'] == False and list_pids[index] != 1 and list_pids[index] != 2):
        list_clear_pids.append(psutil.Process(list_pids[index]))
    else:
        if response_one['action'] == True:
            del list_pid_one[response_one['index']]
        if response_two['action'] == True:
            del list_pid_two[response_two['index']]

json_pid_clasifed = {}
json_uid = 0
print("Toatal process: "+str(len(list_clear_pids)))
for pid in list_clear_pids:
    process = psutil.Process(pid.pid).children(recursive=False)
    if process != []:
        json = {
            "padre": pid,
            "hijos": process,
            "exe_padre": pid.cmdline()
        }
        json_pid_clasifed[json_uid] = json
        json_uid += 1
        for chield in process:
            list_clear_pids.remove(chield)
    else:
        json = {
            "padre": pid,
            "exe_padre": pid.cmdline()
        }
        json_pid_clasifed[json_uid] = json
        json_uid += 1

process_count = 0
for process in range(len(json_pid_clasifed)):
    print("padre: \t"+str(json_pid_clasifed[process]['padre']))
    print("exe padre: \t"+str(json_pid_clasifed[process]['exe_padre']))
    process_count+=1
    if 'hijos'in json_pid_clasifed[process]:
        print("\t\thijos:")
        for chield in json_pid_clasifed[process]['hijos']:
            print(chield)
        process_count += len(json_pid_clasifed[process]['hijos'])
print("Toatal process: "+str(process_count))