import psutil

# Se requiere:
# lista de pid
# lista de PID del proceso 0
# lista de PID del proceso 1
# lista de PID del proceso 2
# exluir los PID de los procesos 0 1 2
# filtar PID por procesos padre
# Obtener los datos del proceso


class Profile:
    __list_pids = []
    __list_pid_one = []
    __list_pid_two = []
    list_clear_pids = []
    json_pid_clasifed = {}
    def __init__(self):
        # lista de procesos
        self.__list_pids = psutil.pids()
        # lista de procesos 1
        self.__list_pid_one = psutil.Process(1).children(recursive=False)
        # lista de procesos 2
        self.__list_pid_two = psutil.Process(2).children(recursive=False)
        # lista de PID limpios
        self.list_clear_pids = []
        self.json_pid_clasifed = {}
        self.__init_pids()
        self.__clasificPIDs()
#Iniciamos el proceso de bsuqued y limpieza de los PID para los procesos 0 1 2
    def __init_pids(self):
        for index in range(len(self.__list_pids)):
            response_one = self.__search(self.__list_pid_one, self.__list_pids[index])
            response_two = self.__search(self.__list_pid_two, self.__list_pids[index])
            if (response_two['action'] == False and response_one['action'] == False and self.__list_pids[index] != 1 and self.__list_pids[index] != 2):
                self.list_clear_pids.append(psutil.Process(self.__list_pids[index]))
            else:
                if response_one['action'] == True:
                    del self.__list_pid_one[response_one['index']]
                if response_two['action'] == True:
                    del self.__list_pid_two[response_two['index']]
#Busqueda en la lista de la existencia de un PID para detectar duplicidad al momento de limpiar los PID
    def __search(self, list, PID):
            dato = 0
            for index in range(len(list)):
                dato = index
                if list[index].pid == PID:
                    return {"action": True, "index": dato}
            return {"action": False, "index": dato}
#Clasificacion de PIDs
    def __clasificPIDs(self):
        json_uid = 0
        print("Toatal process: "+str(len(self.list_clear_pids)))
        for pid in self.list_clear_pids:
            process = psutil.Process(pid.pid).children(recursive=False)
            if process != []:
                json = {
                    "padre": pid,
                    "hijos": process,
                    "exe_padre": pid.cmdline()
                }
                self.json_pid_clasifed[json_uid] = json
                json_uid += 1
                for chield in process:
                    self.list_clear_pids.remove(chield)
            else:
                json = {
                    "padre": pid,
                    "exe_padre": pid.cmdline()
                }
                self.json_pid_clasifed[json_uid] = json
                json_uid += 1
#Show PIDs clasified
    def showPIDs(self):
        process_count = 0
        for process in range(len(self.json_pid_clasifed)):
            print("padre: \t"+str(self.json_pid_clasifed[process]['padre']))
            print("exe padre: \t"+str(self.json_pid_clasifed[process]['exe_padre']))
            process_count+=1
            if 'hijos'in self.json_pid_clasifed[process]:
                print("\t\thijos:")
                for chield in self.json_pid_clasifed[process]['hijos']:
                    print(chield)
                process_count += len(self.json_pid_clasifed[process]['hijos'])
        print("Toatal process: "+str(process_count))
#Reload list clasified
    def reload(self):
        self.__init_pids()

    def test(self):
        result = []
        result = psutil.Process(1).children(recursive=False)
        print(result)
        for pid in result:
            if pid.pid == 7472:
                print(pid)
                